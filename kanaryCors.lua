--
-- Cross-origin Request Sharing (CORS) implementation for HAProxy Lua host
--
-- CORS RFC:
-- https://www.w3.org/TR/cors/
--
-- Copyright (c) 2019. Nick Ramirez <nramirez@haproxy.com>
-- Copyright (c) 2019. HAProxy Technologies, LLC.

function contains(items, test_str)
    for _,item in pairs(items) do
      if item == test_str then
        return true
      end
    end
  
    return false
  end
  
  function get_allowed_origin(origin, allowed_origins)
    if origin ~= nil then
      local allowed_origins = core.tokenize(allowed_origins, ",")
  
      for index, value in ipairs(allowed_origins) do
        allowed_origins[index] = value:gsub("%s+", "")
      end
  
      if contains(allowed_origins, "*") then
        return "*"
      elseif contains(allowed_origins, origin:match("//([^/]+)")) then
        return origin
      end
    end
  
    return nil
  end
  
  function preflight_request_ver1(txn, allowed_methods, allowed_headers)
    txn.http:res_set_header("Access-Control-Allow-Methods", allowed_methods)
    txn.http:res_set_header("Access-Control-Allow-Headers", allowed_headers)
    txn.http:res_set_header("Access-Control-Max-Age", 600)
    txn.http:res_set_header("Access-Control-Allow-Credentials","true")
  end

  function preflight_request_ver2(txn, origin, allowed_methods, allowed_origins, allowed_headers)
  
    local reply = txn:reply()
    reply:set_status(204, "No Content")
    reply:add_header("Content-Type", "text/html")
    reply:add_header("Access-Control-Allow-Methods", allowed_methods)
    reply:add_header("Access-Control-Allow-Headers", allowed_headers)
    reply:add_header("Access-Control-Allow-Credentials","true")
    reply:add_header("Access-Control-Max-Age", 600)
  
    local allowed_origin = get_allowed_origin(origin, allowed_origins)
  
    if allowed_origin == nil then
      core.Debug("CORS: " .. origin .. " not allowed")
    else
      core.Debug("CORS: " .. origin .. " allowed")
      reply:add_header("Access-Control-Allow-Origin", allowed_origin)
    end
  
    txn:done(reply)
  end
  
  function cors_request(txn, allowed_methods, allowed_origins, allowed_headers)
    local headers = txn.http:req_get_headers()
    local transaction_data = {}
    local origin = nil
    
    if headers["origin"] ~= nil and headers["origin"][0] ~= nil then
      origin = headers["origin"][0]
    end
    
    transaction_data["origin"] = origin
    transaction_data["allowed_methods"] = allowed_methods
    transaction_data["allowed_origins"] = allowed_origins
    transaction_data["allowed_headers"] = allowed_headers
  
    txn:set_priv(transaction_data)
  
    local method = txn.sf:method()
    transaction_data["method"] = method
  
    if method == "OPTIONS" and txn.reply ~= nil then
      preflight_request_ver2(txn, origin, allowed_methods, allowed_origins, allowed_headers)
    end
  end
  
  function cors_response(txn)
    local transaction_data = txn:get_priv()
  
    if transaction_data == nil then
      return
    end
    
    local origin = transaction_data["origin"]
    local allowed_origins = transaction_data["allowed_origins"]
    local allowed_methods = transaction_data["allowed_methods"]
    local allowed_headers = transaction_data["allowed_headers"]
    local method = transaction_data["method"]
  
    txn.http:res_add_header("Vary", "Accept-Encoding,Origin")
  
    if origin == nil or origin == '' then
      return
    end
  
    local allowed_origin = get_allowed_origin(origin, allowed_origins)
  
    if allowed_origin == nil then
    else
      if method == "OPTIONS" and txn.reply == nil then
        preflight_request_ver1(txn, allowed_methods, allowed_headers)
      end
      
      txn.http:res_set_header("Access-Control-Allow-Origin", allowed_origin)
    end
  end
  
  core.register_action("cors", {"http-req"}, cors_request, 3)
  core.register_action("cors", {"http-res"}, cors_response, 0)